## SES SMTP password generator

Simple Reactjs app to convert IAM credentials' secret access key to
SMTP password that can be used to authenticate againsts AWS SES.

The application is a web frontend for the python script provided in
the AWS docs: <https://docs.aws.amazon.com/ses/latest/DeveloperGuide/smtp-credentials.html#smtp-credentials-convert>.
