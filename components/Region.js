import React from 'react';
import InputLabel from '@material-ui/core/InputLabel';
import FormHelperText from '@material-ui/core/FormHelperText';
import NativeSelect from '@material-ui/core/NativeSelect';

const SMTP_REGIONS = {
  'us-east-2': 'US East (Ohio)',
  'us-east-1': 'US East (N. Virginia)',
  'us-west-2': 'US West (Oregon)',
  'ap-south-1': 'Asia Pacific (Mumbai)',
  'ap-northeast-2': 'Asia Pacific (Seoul)',
  'ap-southeast-1': 'Asia Pacific (Singapore)',
  'ap-southeast-2': 'Asia Pacific (Sydney)',
  'ap-northeast-1': 'Asia Pacific (Tokyo)',
  'ca-central-1': 'Canada (Central)',
  'eu-central-1': 'Europe (Frankfurt)',
  'eu-west-1': 'Europe (Ireland)',
  'eu-west-2': 'Europe (London)',
  'sa-east-1': 'South America (Sao Paulo)',
  'us-gov-west-1': 'AWS GovCloud (US)',
};

export default function Region({ defaultValue, onChangeHandler }) {
  return <>
    <InputLabel htmlFor='aws-region'>AWS Region</InputLabel>
    <NativeSelect
      required
      id='aws-region'
      onChange={onChangeHandler}
      fullWidth
    >
      {Object.keys(SMTP_REGIONS).map(function (region) {
        return <option
          key={region}
          value={region}
          selected={defaultValue == region}
        >
          {SMTP_REGIONS[region]}
        </option>;
      })}
    </NativeSelect>
    <FormHelperText>The AWS Region where the SMTP password will be used.</FormHelperText>
  </>;
}
