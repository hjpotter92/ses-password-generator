import Document, { Html, Head, Main, NextScript } from 'next/document';
import Container from '@material-ui/core/Container';

class AppDocument extends Document {
  static async getInitialProps(ctx) {
    const initialProps = await Document.getInitialProps(ctx);
    return { ...initialProps };
  }

  render() {
    return (
      <Html lang="en">
        <Head>
          <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap" />
        </Head>
        <body>
          <Container maxWidth='sm'>
            <Main />
            <NextScript />
          </Container>
        </body>
      </Html>
    );
  }
}

export default AppDocument;
