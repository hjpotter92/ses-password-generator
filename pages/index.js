import React, { useState } from 'react';
import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';
import TextField from '@material-ui/core/TextField';
import SecurityIcon from '@material-ui/icons/Security';
import RoomIcon from '@material-ui/icons/Room';
import Region from '../components/Region';
import PasswordModal from '../components/PasswordModal';

export default function Index() {
  const [region, setRegion] = useState('us-east-1');
  const [secretAccessKey, setSecretAccessKey] = useState('');
  // const [modalOpen, setModalOpen] = useState(false);

  return <>
    <Grid container spacing={4} alignItems='center'>
      <Grid container item spacing={2} alignItems='center' justify='center'>
        <Grid item>
          <RoomIcon fontSize='large' />
        </Grid>
        <Grid item>
          <Region
            defaultValue={region}
            onChangeHandler={e => setRegion(e.target.value)} />
        </Grid>
      </Grid>
      <Grid container item spacing={2} alignItems='center' justify='center'>
        <Grid item>
          <SecurityIcon fontSize='large' />
        </Grid>
        <Grid item>
          <TextField
            label='AWS Secret Access Key'
            helperText='The Secret Access Key to convert.'
            fullWidth
            placeholder={secretAccessKey && secretAccessKey || 'AWS_SECRET_ACCESS_KEY'}
            onChange={e => setSecretAccessKey(e.target.value)}
          />
        </Grid>
      </Grid>
    </Grid>
    {/* <Button onClick={(e) => secretAccessKey && setModalOpen(true)}>Compute password!</Button> */}
    <PasswordModal
      region={region}
      secretAccessKey={secretAccessKey}
    />
  </>;
}
