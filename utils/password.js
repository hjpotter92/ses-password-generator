import { createHmac } from 'crypto';

const DATE = "11111111";
const SERVICE = "ses";
const MESSAGE = "SendRawEmail";
const TERMINAL = "aws4_request";
const VERSION = new Buffer.from([0x04]);

function sign(key, message) {
  let hmac = new createHmac('sha256', key, { encoding: 'utf-8' });
  hmac.update(message);
  return hmac.digest();
}

function compute_password(secretAccessKey, region) {
  let signature = sign(`AWS4${secretAccessKey}`, DATE);
  signature = sign(signature, region);
  signature = sign(signature, SERVICE);
  signature = sign(signature, TERMINAL);
  signature = sign(signature, MESSAGE);
  let versioned_signature = Buffer.concat([VERSION, signature]);
  return versioned_signature.toString('base64');
}

module.exports = {
  compute_password
}
