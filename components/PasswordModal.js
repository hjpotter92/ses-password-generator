import React, { useState } from 'react';
import Button from '@material-ui/core/Button';
import Modal from '@material-ui/core/Modal';
import { compute_password } from '../utils/password';

export default function PasswordModal({ region, secretAccessKey }) {
  const [open, setOpen] = useState(false);

  const handleOpen = () => {
    setOpen(true);
  };
  const handleClose = () => {
    setOpen(false);
  };

  return <>
    <Button onClick={handleOpen}>Compute password!</Button>
    <Modal
      open={open}
      onClose={handleClose}
    >
      <div>
        The SMTP password is: {compute_password(secretAccessKey, region)}.
      </div>
    </Modal>
  </>;
}
